__author__ = 'Miguel Rivero'

try:
    import SimpleGUICS2Pygame.simpleguics2pygame as simplegui
except:
    import simplegui
import math
import random

# globals for user interface
WIDTH = 800
HEIGHT = 600
score = 0
lives = 3
time = 0.5
started = False


class ImageInfo:
    """
    Sprite class
    """
    def __init__(self, center, size, radius=0, lifespan=None, animated=False):
        """(center, size, radius = 0, lifespan = None, animated = False)

        :param center: list
        :param size: list
        :param radius: integer
        :param lifespan: float
        :param animated: boolean
        :return: Initialized ImageInfo object
        """
        self.center = center
        self.size = size
        self.radius = radius
        if lifespan:
            self.lifespan = lifespan
        else:
            self.lifespan = float('inf')
        self.animated = animated

    def get_center(self):
        """
        Get attribute center
        """
        return self.center

    def get_size(self):
        """
        Get attribute size
        """
        return self.size

    def get_radius(self):
        """
        Get attribute radius
        """
        return self.radius

    def get_lifespan(self):
        """
        Get attribute lifespan
        """
        return self.lifespan

    def get_animated(self):
        """
        Get attribute animated
        """
        return self.animated


# art assets created by Kim Lathrop, may be freely re-used in non-commercial projects, please credit Kim

# debris images - debris1_brown.png, debris2_brown.png, debris3_brown.png, debris4_brown.png
#                 debris1_blue.png, debris2_blue.png, debris3_blue.png, debris4_blue.png, debris_blend.png
debris_info = ImageInfo([320, 240], [640, 480])
debris_image = simplegui.load_image("http://images/debris2_blue.png")

# nebula images - nebula_brown.png, nebula_blue.png
nebula_info = ImageInfo([400, 300], [800, 600])
nebula_image = simplegui.load_image("http://images/nebula_blue.s2014.png")

# splash image
splash_info = ImageInfo([200, 150], [400, 300])
splash_image = simplegui.load_image("http://images/splash.png")

# ship image
ship_info = ImageInfo([45, 45], [90, 90], 35)
ship_image = simplegui.load_image("http://images/double_ship.png")

# missile image - shot1.png, shot2.png, shot3.png
missile_info = ImageInfo([5, 5], [10, 10], 3, 50)
missile_image = simplegui.load_image("http://images/shot2.png")

# asteroid images - asteroid_blue.png, asteroid_brown.png, asteroid_blend.png
asteroid_info = ImageInfo([45, 45], [90, 90], 40)
asteroid_image = simplegui.load_image("http://images/asteroid_blue.png")

# animated explosion - explosion_orange.png, explosion_blue.png, explosion_blue2.png, explosion_alpha.png
explosion_info = ImageInfo([64, 64], [128, 128], 17, 24, True)
explosion_image = simplegui.load_image("http://images/explosion_alpha.png")

# sound assets
soundtrack = simplegui.load_sound("http://sounds/soundtrack.ogg")
missile_sound = simplegui.load_sound("http://sounds/missile.ogg")
missile_sound.set_volume(.5)
ship_thrust_sound = simplegui.load_sound("http://sounds/thrust.ogg")
# explosion_sound = simplegui.load_sound("http://sounds/explosion.ogg")


# helper functions to handle transformations
def angle_to_vector(ang):
    """(float) -> list(float)

    It returns the unit vector that corresponds to an angle
    """
    return [math.cos(ang), math.sin(ang)]


def dist(p, q):
    """(list(float), list(float)) -> float

    Calculate euclidean distance between to points

    >>> dist([0, 0], [0, 1])
    1
    >>> dist([1, 0], [1, 4])
    4
    """
    return math.sqrt((p[0] - q[0]) ** 2+(p[1] - q[1]) ** 2)


class Ship:
    """
    Ship class
    """
    def __init__(self, pos, vel, angle, image, info):
        """(position, velocity, angle, image, info)

        :param pos: list of int
        :param vel: list of int
        :param angle: float
        :param image:
        :param info:
        :return: Initialized Ship object
        """
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.thrust = False
        self.angle = angle
        self.angle_vel = 0
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()

    def draw(self, canvas):
        """
        Draw the ship depending if it`s thrust
        """
        if self.thrust:
            ship_image_center_thrusted =[self.image_center[0] + self.image_size[0],
                                         self.image_center[1]]
            canvas.draw_image(self.image, ship_image_center_thrusted, self.image_size, self.pos, self.image_size, self.angle)
        else:
            canvas.draw_image(self.image, self.image_center, self.image_size, self.pos, self.image_size, self.angle)

    def update(self):
        """
        Updating the ship position and velocity
        """
        # update position and angle
        self.pos[0], self.pos[1] = (self.pos[0] + self.vel[0]) % WIDTH, (self.pos[1] + self.vel[1]) % HEIGHT
        self.angle += self.angle_vel
        # the forward vector pointing in the direction the ship is facing based on the ship's angle.
        if self.thrust:
            acceleration_vector = angle_to_vector(self.angle)
            self.vel[0] += acceleration_vector[0] * 0.1
            self.vel[1] += acceleration_vector[1] * 0.1

        self.vel[0], self.vel[1] = self.vel[0] * 0.99, self.vel[1] * 0.99

    def increment_angle_vel(self):
        """
        Controlling the ship`s angle clockwise
        """
        self.angle_vel += 0.05

    def decrement_angle_vel(self):
        """
        Controlling the ship`s angle anticlockwise
        """
        self.angle_vel -= 0.05

    def set_thrust(self, boolean):
        """
        Set thrust on or off
        """
        self.thrust = boolean
        if self.thrust:
            ship_thrust_sound.play()
        else:
            ship_thrust_sound.pause()
            ship_thrust_sound.rewind()

    def shoot(self):
        """
        Controlling the missiles
        """
        global missile_group
        forward = angle_to_vector(self.angle)
        c = 5
        new_missile_pos = [self.pos[0] + forward[0] * (self.image_size[0]/2), self.pos[1] + forward[1] * (self.image_size[1]/2)]
        new_missile_vel = [self.vel[0] + c * forward[0], self.vel[1] + c * forward[1]]
        new_missile = Sprite(new_missile_pos, new_missile_vel, self.angle, 0, missile_image, missile_info, missile_sound)
        missile_group.add(new_missile)

    def restart(self):
        """
        Restore the ship
        """
        self.pos = [WIDTH / 2, HEIGHT / 2]
        self.vel = [0, 0]
        self.thrust = False
        self.angle = 0
        self.angle_vel = 0


class Sprite:
    """
    Sprite class
    """
    def __init__(self, pos, vel, ang, ang_vel, image, info, sound=None):
        """ (pos, vel, ang, ang_vel, image, info, sound = None)

        :param pos: list of int
        :param vel: list of int
        :param ang: int
        :param ang_vel: int
        :param image:
        :param info:
        :param sound: sound instance
        :return: Initialized Sprite object
        """
        self.pos = [pos[0], pos[1]]
        self.vel = [vel[0], vel[1]]
        self.angle = ang
        self.angle_vel = ang_vel
        self.image = image
        self.image_center = info.get_center()
        self.image_size = info.get_size()
        self.radius = info.get_radius()
        self.lifespan = info.get_lifespan()
        self.animated = info.get_animated()
        self.age = 0
        if sound:
            sound.rewind()
            sound.play()

    def draw(self, canvas):
        """
        Draw sprites
        """
        canvas.draw_image(self.image, self.image_center, self.image_size, self.pos, self.image_size, self.angle)

    def update(self):
        """ (self) -> boolean

        Updating the sprite position and velocity. It returns True if it should be destroyed.
        """
        autodestroy = False
        self.pos[0], self.pos[1] = (self.pos[0] + self.vel[0]) % WIDTH, (self.pos[1] + self.vel[1]) % HEIGHT
        self.angle += self.angle_vel
        self.age += 1
        if self.age >= self.lifespan:
            autodestroy = True
        return autodestroy

    def collide(self, other_object):
        """(self, object) -> boolean

        It returns True if there is collision, False otherwise.
        """
        collide = False
        if dist(self.pos, other_object.pos) < self.radius + other_object.radius:
            collide = True
            exp = simplegui.load_sound("http://sounds/explosion.ogg")
            exp.play()
        return collide


def draw(canvas):
    """
    Draw handler for the canvas
    """
    global time, score, lives, started

    # animiate background
    time += 1
    wtime = (time / 4) % WIDTH
    center = debris_info.get_center()
    size = debris_info.get_size()
    canvas.draw_image(nebula_image, nebula_info.get_center(), nebula_info.get_size(), [WIDTH / 2, HEIGHT / 2], [WIDTH, HEIGHT])
    canvas.draw_image(debris_image, center, size, (wtime - WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))
    canvas.draw_image(debris_image, center, size, (wtime + WIDTH / 2, HEIGHT / 2), (WIDTH, HEIGHT))

    if lives < 1:
        started = False
        soundtrack.pause()
        soundtrack.rewind()
        for rock in set(rock_group):
            rock_group.discard(rock)

    if not started:
        canvas.draw_image(splash_image, splash_info.get_center(),
                          splash_info.get_size(), [WIDTH / 2, HEIGHT / 2],
                          splash_info.get_size())
        my_ship.restart()
    else:
        # draw and update ship
        my_ship.update()
        my_ship.draw(canvas)
        if group_collide(rock_group, my_ship):
            lives -= 1
        # draw and update rocks and missiles
        process_sprite_group(canvas, rock_group)
        process_sprite_group(canvas, missile_group)

        number = group_group_collide(rock_group, missile_group)
        score += 1 * number

    #draw lives and score
    canvas.draw_text('Lives', [30, 50], 30, 'Green')
    canvas.draw_text(str(lives), [30, 80], 30, 'Green')
    canvas.draw_text('Score', [600, 50], 30, 'Red')
    canvas.draw_text(str(score), [600, 80], 30, 'Red')


def process_sprite_group(canvas, sprite_set):
    """
    draw a group of sprites
    """
    for sprite in set(sprite_set):
        sprite.draw(canvas)
        if sprite.update():
            sprite_set.remove(sprite)


# timer handler that spawns a rock
def rock_spawner():
    """
    timer handler that spawns a rock while the rocks' number is less than 12
    """
    global rock_group
    new_position = [random.randrange(WIDTH), random.randrange(HEIGHT)]
    if len(rock_group) < 12 and started and dist(new_position, my_ship.pos) > my_ship.radius * 3:
        new_velocity = [random_from_to(-1, 1), random_from_to(-1, 1)]
        new_angle_vel = random_from_to(-0.13, 0.13)
        new_rock = Sprite(new_position, new_velocity, 0, new_angle_vel, asteroid_image, asteroid_info)
        rock_group.add(new_rock)


def click(pos):
    """
    mouseclick handlers that reset UI and conditions whether
    splash image is drawn
    """
    global started, score, lives
    center = [WIDTH / 2, HEIGHT / 2]
    size = splash_info.get_size()
    in_width = (center[0] - size[0] / 2) < pos[0] < (center[0] + size[0] / 2)
    in_height = (center[1] - size[1] / 2) < pos[1] < (center[1] + size[1] / 2)
    if (not started) and in_width and in_height:
        started = True
        lives = 3
        score = 0
        soundtrack.play()


def group_collide(group, other_object):
    """
    Manage collisions between a sprite and a group of sprites
    """
    collision = False
    for a_object in set(group):
        if a_object.collide(other_object):
            collision = True
            group.remove(a_object)
    return collision


def group_group_collide(group1, group2):
    """
    Manage collisions between two group of sprites
    """
    number_of_collisions = 0
    for sprite in set(group1):
        if group_collide(group2, sprite):
            number_of_collisions += 1
            group1.discard(sprite)
    return number_of_collisions


def random_from_to(lower, upper):
    """(float, float) -> float

    Return a random float between lower and upper. lower can be Negative
    """
    if lower > upper:
        lower, upper = upper, lower
    lower = float(lower)
    upper = float(upper)
    range_width = upper - lower
    return random.random() * range_width + lower


def keydown(key):
    """
    keydown handlers to control ship
    """
    if key == simplegui.KEY_MAP['left']:
        my_ship.decrement_angle_vel()
    elif key == simplegui.KEY_MAP['right']:
        my_ship.increment_angle_vel()
    elif key == simplegui.KEY_MAP['up']:
        my_ship.set_thrust(True)
    elif key == simplegui.KEY_MAP['space']:
            my_ship.shoot()


def keyup(key):
    """
    keyup handlers to control ship
    """
    if key == simplegui.KEY_MAP['left']:
        my_ship.increment_angle_vel()
    elif key == simplegui.KEY_MAP['right']:
        my_ship.decrement_angle_vel()
    elif key == simplegui.KEY_MAP['up']:
        my_ship.set_thrust(False)

# initialize frame
frame = simplegui.create_frame("Asteroids", WIDTH, HEIGHT)

# initialize ship and two groups sprites( rocks and missiles)
my_ship = Ship([WIDTH / 2, HEIGHT / 2], [0, 0], 0, ship_image, ship_info)
rock_group = set()
missile_group = set()

# register handlers
frame.set_draw_handler(draw)
frame.set_keyup_handler(keyup)
frame.set_keydown_handler(keydown)
frame.set_mouseclick_handler(click)
timer = simplegui.create_timer(1000.0, rock_spawner)

# get things rolling
timer.start()
frame.start()
